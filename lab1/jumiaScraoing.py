from datetime import datetime
import requests
from bs4 import BeautifulSoup

URL = "https://www.jumia.ma/casseroles-marmites/"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")
#print(soup)
results = soup.findAll("a", class_="core")
today = datetime.now()
date_time = today.strftime("%m_%d_%Y_%H_%M_%S")
f = open("SCRAP_" + date_time + ".txt", "x")
for job_element in results:
    name = job_element.find("h3", class_="name")
    price = job_element.find("div", class_="prc")
    oldPrice = job_element.find("div", class_="old")
    discountPer = job_element.find("div", class_="tag _dsct _sm")
    #textResult="-----------------------------------------------------------------------------\n+PRODUCT NAMES\n"+name.text+"\nPRODUCT PRICE\n"+price.text+"\nPRODUCT OLD PRICE\n"+oldPrice.text+"\nDISCOUNT PRICE\n"+discountPer.text+"\n"
    f.write("-----------------------------------------------------------------------------\n")
    f.write("PRODUCT NAMES\n")
    f.write(name.text+"\n")
    f.write("PRODUCT PRICE\n")
    f.write(price.text+"\n")
    f.write("PRODUCT OLD PRICE\n")
    if not oldPrice:
        f.write(oldPrice.text+"\n")
    else:
        f.write("0 Dhs\n")
    f.write("DISCOUNT PRICE\n")
    if not oldPrice:
        f.write(discountPer.text+"\n")
    else:
        f.write("0%\n")


f.close()


f = open("demofile3.txt", "r")
print(f.read())